package client;

import dto.ProductRequest;
import entity.Category;
import entity.Product;
import service.ProductService;
import service.ProductServiceImpl;

import java.util.List;
import java.util.Scanner;

public class InventoryManagementSystem {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        // addProduct
        // show all product
        // notify low stock
        // display all category

        System.out.println("Inventory Management System: ");
        System.out.println("1. Add product");
        System.out.println("2. Display all product");
        System.out.println("3. Notify for low stock product");
        System.out.println("4. Add category");
        System.out.println("5. Display all categories");

        int choice = scanner.nextInt();

        switch (choice) {
            case 1:
                createProduct(scanner);
                break;
            case 2:
                displayAllProduct();
                break;
            case 3:
                notfyLowStock();
                break;
            case 4:
                createCategory(scanner);
                break;
        }
    }

    private static void createCategory(Scanner sc) {
        System.out.println("Ju lutem fusni emrin e kategorse");
        Category category = new Category();
        category.setTitle(sc.next());
        ProductService productService = new ProductServiceImpl();
        productService.createCategory(category);
    }

    private static void displayAllProduct() {
        ProductService productService = new ProductServiceImpl();
    }

    private static void notfyLowStock() {
        ProductService productService = new ProductServiceImpl();
        List<Product> productList = productService.notifyForLowStock();
        for (Product product :productList) {
            System.out.println(product);
        }
    }

    public static void createProduct(Scanner scanner){
        ProductRequest productRequest = new ProductRequest();
        System.out.println("Set title: ");
        productRequest.setTitle(scanner.next());
        System.out.println("Set description: ");
        productRequest.setDescription(scanner.next());
        System.out.println("Set quantity: ");
        productRequest.setQuantity(scanner.nextInt());
        System.out.println("Set Id of categorise: ");
        productRequest.setCategoryId(scanner.nextLong());
        System.out.println("Set price: ");
        productRequest.setPrice(scanner.nextDouble());
        ProductService productService = new ProductServiceImpl();
        productService.createProduct(productRequest);
    }


}
