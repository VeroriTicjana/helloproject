package service;

import config.HibernateConfig;
import converter.ProductConverter;
import dao.CategoryDao;
import dao.CategoryDaoImpl;
import dao.ProductDao;
import dao.ProductDaoImpl;
import dto.ProductRequest;
import entity.Category;
import entity.Product;
import org.hibernate.SessionFactory;
import vaidator.ProductValidator;

import java.util.ArrayList;
import java.util.List;

public class ProductServiceImpl implements ProductService{
    private final SessionFactory sessionFactory = HibernateConfig.getSessionFactory();
    private final ProductDao productDao = new ProductDaoImpl(sessionFactory);
    private final CategoryDao categoryDao = new CategoryDaoImpl(sessionFactory);
    private final Integer limitStock = 5;
    public List<Product> notifyForLowStock() {
        List<Product> products = productDao.findAll();
        List<Product> lowStock = new ArrayList<>();
        for (Product product : products) {
            if (product.getQuantity() < limitStock) {
                lowStock.add(product);
            }
        }
        return lowStock;
    }

    @Override
    public void createCategory(Category category) {
        categoryDao.save(category);
    }

    @Override
    public void createProduct(ProductRequest request) {
        ProductValidator.validateProduct(request);
        Product product = ProductConverter.convertRequestToEntity(request);
        productDao.save(product);
        System.out.println(request);
    }
}